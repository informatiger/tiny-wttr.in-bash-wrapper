#!/bin/sh

# Usage info
show_help() {
cat << EOF
Usage: ${0##*/} [-h] -L Location [-n] [-1] [-2] [-q] [-Q] [-T] [-m] [-u] [-M]
Fetches weather information from wttr.in

    -h          display this help and exit
    -L Location Name of the Location
    -n          Less information
    -1          Show one day
    -2          Show two days
    -q          quiet
    -Q          more quiet
    -T          disable colors
    -m          metric
    -u          uscs (used by default in the us)
    -M          wind speed in m/s
EOF
}

# Variables

location=""
args=""

# Reset getopt
OPTIND=1

# Get options
while getopts hn12qQTmuML: opt; do
    case $opt in
    h)
        show_help
        exit 0
        ;;
    L)
        location=$OPTARG
        ;;
    n)  args=$args:"n"
        ;;
    1)  args=$args:"1"
        ;;
    2)  args=$args:"2"
        ;;
    q)  args=$args:"q"
        ;;
    Q)  args=$args:"Q"
        ;;
    T)  args=$args:"T"
        ;;
    m)  args=$args:"m"
        ;;
    u)  args=$args:"u"
        ;;
    M)  args=$args:"M"
        ;;
    *)
        show_help >&2
        exit 1
        ;;
    esac
done

# Discard the options and sentinel
shift "$((OPTIND-1))"

# If location empty then show help
if [ -z "$location" ]; then
  show_help >&2
  exit 1
fi

# Send request to wttr.in
curl -H "Accept-Language: ${LANG%_*}" "https://wttr.in/$location?$args"
